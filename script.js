/*
1) Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

Прототипне наслідування дозволяє об'єктам успадковувати властивості та методи від інших об'єктів.
Кожен об'єкт має внутрішнє посилання на інший об'єкт, який називається його прототипом.
Коли треба отримати доступ до властивостей або методів об'єкту, JavaScript спочатку перевіряє,
чи є ці властивості або методи безпосередньо в самому об'єкті.
Якщо вони не знайдені, JavaScript звертається до прототипу об'єкту і перевіряє, чи є властивості або методи у прототипі.
Цей пошук продовжується по ланцюжку прототипів, поки не буде знайдено відповідну властивість або метод,
або доки не буде досягнутий кінець ланцюжка прототипів.

2) Для чого потрібно викликати super() у конструкторі класу-нащадка?

Виклик super() дозволяє класу-нащадку мати доступ до властивостей та методів, визначених у базовому класі.
Після виклику super(), об'єкт, що представляє клас-нащадок, отримує доступ до властивостей та методів базового класу
через ключове слово this.
*/

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    getName() {
        return this.name;
    }

    setName(name) {
        this.name = name;
    }

    getAge() {
        return this.age;
    }

    setAge(age) {
        this.age = age;
    }

    getSalary() {
        return this.salary;
    }

    setSalary(salary) {
        this.salary = salary;
    }

}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    getSalary() {
        return this.salary * 3;
    }
}

const Dan = new Programmer("Dan", 20, 3000, ["JavaScript", "Python"]);
console.log(Dan.getName());
console.log(Dan.getAge());
console.log(Dan.getSalary());
console.log(Dan.lang);